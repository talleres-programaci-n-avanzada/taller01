# Taller01

## Vectores

Cree un vector `V` de números enteros, de tamaño MAX (aleatoriamente entre 1 y 10) a partir de este vector generar un vector `numero` y un vector `numero_ocurrencias`, donde se debe almacenar los valores del vector con su número de ocurrencias. En el vector `numero` se encuentra los números del vector `V`, una sola vez en orden de aparición en el vector `V`. En el vector `número_ocurrencias[i]` se encuentra el número de veces que se encuentra el `numero[i]` en el vector `V`. 
```text
Por ejemplo, Si el vector `V` es 4 3 6 3 8 2 9 3 2 6 1 3 6 4 7 4 3 8 4 1
El vector `numero` será: 4 3 6 8 2 9 1 7 
El vector `numero_ocurrencias` será:4 5 3 2 2 1 2 1 
```
A partir del vector `V` inicial de tamaño MAX, dejar una sola ocurrencia de los elementos, garantizando que sea la aparición media del elemento. La aparición media de un elemento cumple que existen tantas  ocurrencias  del  elemento  antes  que  él  (incluyéndolo,  si  hay un número par de apariciones) que después de él. 
```text
Si el vector `V` es 6 4 3 4 6 8 4 5 9 6 3 4 9 3 3 5 6 3 9 6 
El vector `V` quedaría 4 8 5 6 9 3
```

## Matrices
Elaborar una función que imprima el valor mínimo de cada columna de una matriz de `i` filas y `j` columnas. Si hay más de una ocurrencia se debe reportar la última encontrada. Se debe imprimir el valor mínimo y los índices donde se encuentra ese valor. 
```text
Por ejemplo, si la función recibió la matriz: 4 3 2 8 3 1 4 2 8 5 3 2 2 1 8 3 
El reporte debe ser: número fila columna 
                       2     4      1 
                       1     4      2 
                       2     1      3 
                       2     3      4 
```
Los valores de `i`, `j` siempre cumplen la siguiente condición: `i`<=100 y `j`<=30 
Los números enteros de la matriz se han generado aleatoriamente y están entre 1 y 10.