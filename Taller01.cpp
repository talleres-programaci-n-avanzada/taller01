#include<iostream>
#include <stdlib.h>
#include <time.h>

using namespace std;

void crear_V(short V[]);
short vector_numero(short numero[], short V[]);
void vector_ocurrencias(short contador, short numero[], short V[], short numero_ocurrencias[]);
void aparicion_media(short contador, short numero[], short V[], short numero_ocurrencias[]);
void crear_M(short matriz[100][30], short i, short j);
void reporte(short matriz[100][30], short i, short j);

int main(){
    srand(time(NULL));
    short i=1 + rand()%100, j=1 + rand()%30;
    short opcion=0, contador;
    short V[20], numero[9], matriz[100][30], numero_ocurrencias[9];
    do{
        cout << "Buen día, seleccione la opción que desee:\n";
        cout << "1. Crear e imprimir un vector 'V' de números enteros de 20 elementos aleatorios.\n";
        cout << "2. Crear e  imprimir  el vector 'numero' donde se encuentra los números del vector 'V', una sola vez en orden de aparición en el vector V.\n";
        cout << "3. Crear e imprimir el vector 'numero_ocurrencias' donde se guarda en cada posición el valor correspondiente.\n";
        cout << "4. Imprimir  el  vector 'V' para  que  visualice la aparición media de cada número en orden.\n";
        cout << "5. Crear e imprimir la matriz de números.\n";
        cout << "6. Generar el reporte de la matriz.\n";
        cout << "7. Salir.\n";
        cout << "Tenga en cuenta que debe generar la matriz o el vector antes de solicitar el siguiente paso.\n";
        cin >> opcion;
        if (opcion ==1){
            crear_V(V);
        }
        if (opcion ==2){
            contador=vector_numero(numero, V);
        }
        if (opcion ==3){
            vector_ocurrencias(contador, numero, V, numero_ocurrencias);
        }
        if (opcion ==4){
            aparicion_media(contador, numero, V, numero_ocurrencias);
        }
        if (opcion == 5){
            crear_M(matriz, i, j);
        }
        if (opcion == 6){
            reporte(matriz, i, j);
        }
    } while (opcion != 7);
}

void crear_V(short V[]){
    for(int i=0; i<20; i++){
        V[i]=1 + rand()%9;
        cout << V[i] << "|";
    }
    cout << "\n\n";
}

short vector_numero(short numero[], short V[]){
    for(int j=0; j<9; j++){
        numero[j]=0;
    }
    
    numero[0]=V[0];
    short contador=0;
    
    for(int i=1; i<20; i++){
        short veces=0;
        for(int j=0; j<=contador; j++){
            if(V[i]==numero[j]){
                veces++;
            }
        }
        if(veces==0){
            contador++;
            numero[contador]=V[i];
        }
    }
    
    for(int i=0; i<=contador; i++){
        cout << numero[i] << " | ";
    }
    cout << "\n\n";
    return contador;
}

void vector_ocurrencias(short contador, short numero[], short V[], short numero_ocurrencias[]){
    for(int i=0; i<=contador; i++){
        numero_ocurrencias[i]=0;
        for(int j=0; j<20; j++){
            if(numero[i]==V[j]){
                numero_ocurrencias[i]++;
            }
        }
        cout << numero_ocurrencias[i] << " | ";
    }
    cout << "\n\n";
}

void aparicion_media(short contador, short numero[], short V[], short numero_ocurrencias[]){
    short aparicion[contador+1];
    for(int i=0; i<=contador; i++){
        if(numero_ocurrencias[i]%2==0){
            aparicion[i]=numero_ocurrencias[i]/2;
        }
        if(numero_ocurrencias[i]%2!=0){
            aparicion[i]=1+numero_ocurrencias[i]/2;
        }
    }
    for(int i=0; i<=contador; i++){
        int j=19;
        while (numero_ocurrencias[i]!=0 && j>=0){
            
            if(V[j]==numero[i] && numero_ocurrencias[i]!=aparicion[i]){
                numero_ocurrencias[i]--;
                V[j]=0;
            }
            if(V[j]==numero[i] && numero_ocurrencias[i]==aparicion[i]){
                numero_ocurrencias[i]--;
            }
            j--;
        }
    }
    for(int i=0; i<20; i++){
        cout << V[i] << " | ";            
    }
    cout << "\n\n";
    
    for(int i=0; i<20; i++){
        while(V[i]==0 && i<=contador){
            for(int k=i; k<19; k++){
                    V[k]=V[k+1];
                    V[19]=0;
            }
        }    
    }
    
    for(int i=0; i<20; i++){
        cout << V[i] << " | ";            
    }
    cout << "\n\n";
}

void crear_M(short matriz[100][30], short i, short j){
    for (int f=0; f<=i; f++){
        for (int c=0; c<=j; c++){
            matriz[f][c]= 1 + rand()%9;
            cout << matriz[f][c] << " ";
        }
        cout << "\n";
    }
    cout << "\n";
}

void reporte(short matriz[100][30], short i, short j){
    short menor, fmenor=0;
    cout << "Número:  |  Fila:  |  Columna:  \n";
    for(int c=0; c<=j; c++){
        menor=matriz[0][c];
        for(int f=1; f<=i; f++){
            if(matriz[f][c]<menor){
                menor=matriz[f][c];
                fmenor=f+1;
            }
            if(matriz[f][c]==menor){
                menor=matriz[f][c];
                fmenor=f+1;
            }
        }
        cout << menor << "       |   " << fmenor << "     |     " << c+1 << "\n"; 
    }
    cout << "\n";
}